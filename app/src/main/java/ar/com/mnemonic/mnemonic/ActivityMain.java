/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import java.util.Locale;

public class ActivityMain extends AppCompatActivity {


    public static Boolean sherlockFixedLength = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        getSupportActionBar().setTitle("");


        //  Reading shared preferences to configure dictionaries to read according the language selected
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ActivityMain.this);
        sherlockFixedLength = preferences.getBoolean("switch_sherlock", false);
        String language = preferences.getString("language", getString(R.string.language_default));

        //  If there is not a language selected because its first time or because preferences were deleted
        //  set phone language as default
        if (language.equals(getString(R.string.language_default))) {
            Locale currentLocale = getResources().getConfiguration().locale;
            String shortTag = currentLocale.getDefault().getLanguage();
            LangHelper.setLanguageFromShortTag(this, shortTag);
            ActivitySettings.SettingsFragment.setSettingsDefaults("language", LangHelper.getLanguage(), this);
        }

        PreferenceManager.setDefaultValues(this, R.xml.root_preferences, false);
        LangHelper.setLanguageAttributes(this, language);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(ActivityMain.this, ActivitySettings.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}

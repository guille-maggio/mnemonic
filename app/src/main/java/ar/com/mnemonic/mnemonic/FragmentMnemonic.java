/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import org.json.JSONException;
import java.util.ArrayList;

import static android.content.Context.CLIPBOARD_SERVICE;
import static ar.com.mnemonic.mnemonic.Mnemonic.inverter;

public class FragmentMnemonic extends Fragment {

    private static ArrayList<ArrayList<String>> outputList;
    private static String[] wordsToPrint;
    private static Integer arraySize;
    private static String outputText = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        outputList = new ArrayList<ArrayList<String>>();
        Bundle bundle = this.getArguments();

        //  Get all the info from main fragment passed through bundle
        //  The info consist of a number of array lists
        //  each array list consist of the words corresponding to each piece number
        //  For example  the first array could have all the words for '123' piece
        //  ie: ["aquaphobia", "ecophobia", "goof-up", "oecophobia", "oikophobia"]

        if (bundle != null) {
            arraySize = bundle.getInt("size");
            for (int i = 0; i < arraySize; i++) {
                outputList.add(bundle.getStringArrayList("item" + i));
            }
            wordsToPrint = new String[arraySize];
        }

    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mnemonic, container, false);
    }

    public void setText(String text, String originalN) {
        // This will show the output, ie, only the selected words of each spinner
        TextView t = getView().findViewById(R.id.mnemonic_result);
        t.setText(text.toUpperCase());
    }

    public String getText(String text) {
        TextView t = getView().findViewById(R.id.mnemonic_result);
        return t.getText().toString();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        view.setBackgroundResource(R.drawable.top_background);


        // This correspond button will copy the output words into clipboard
        FloatingActionButton copyButton = view.findViewById(R.id.mnemonic_copy);
        copyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(getString(R.string.app_name), outputText.toUpperCase());
                clipboard.setPrimaryClip(clip);
                Snackbar snack = Snackbar.make(view, getString(R.string.to_clipboard), Snackbar.LENGTH_SHORT);
                snack.show();
            }
        });

        //  This layout is populated automatically with all the spinners containing the output words
        //  Eeach spinner will cointain the whole array list of strings for the associated pice number
        LinearLayout layout = getActivity().findViewById(R.id.spinner_layout);
        for (int i = 0; i < arraySize; i++) {

            // This code gets the first word of the arraylist and recalculate the original piece number
            //  in order to show it along side with the associated spinner
            outputList.get(i).get(0);
            String originNumber = "";
            originNumber = outputList.get(i).get(0);
            try {
                originNumber = inverter(originNumber, LangHelper.getGraphemes());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //

            LinearLayout container = (LinearLayout) View.inflate(getContext(), R.layout.custom_spinner_label_container, null);
            final TextView label = container.findViewById(R.id.mnemonic_label);
            final Spinner spinner = container.findViewById(R.id.mnemonic_spinner);
            container.setOrientation(LinearLayout.HORIZONTAL);
            LayoutParams linearlayoutlayoutparams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            container.setLayoutParams(linearlayoutlayoutparams);
            label.setText(originNumber);
            spinner.setId(i);

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), R.layout.custom_spinner_item_selected, outputList.get(i));
            spinner.setAdapter(spinnerArrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                //  Every time a item is selected over a spinner
                //  Output Textview must be updated with the new word selected replacing the old one
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    int spinnerId = spinner.getId();
                    String spinnerText = spinner.getSelectedItem().toString();

                    wordsToPrint[spinnerId] = spinnerText;
                    outputText = "";
                    for (int i = 0; i < wordsToPrint.length; i++) {
                        outputText = outputText + wordsToPrint[i] + " ";
                    }
                    String original = "";
                    try {
                        original = inverter(outputText, LangHelper.getGraphemes());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    setText(outputText, original);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            //Populate the layout
            layout.addView(container);
        }

        view.findViewById(R.id.back_to_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int movement = TouchHelper.checkMovement(event);
                switch (movement) {
                    case (TouchHelper.DOWN_TO_UP):
                        view.findViewById(R.id.back_to_main).performClick();
                        return true;
                    default:
                        return true;
                }
            }
        });
    }
}

/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.content.Context;
import org.json.JSONException;
import org.json.JSONObject;

import static ar.com.mnemonic.mnemonic.Mnemonic.getJsonFromAssets;

public class LangHelper {
    //  Class with Global variables that set assets that app should read depending on chosen language

    private static String phonemesByLanguageFileName = "phonemes-by-language.json";
    private static String fullDictFileName = "dictionary-english-std.json";
    private static String graphemesFileName = "graphemes-english-std.json";
    private static String languageRulesTag = "english_rules_std";
    private static String languageTag = "english_std";
    private static String languageTitle = "(EN)";
    private static String language = "";
    private static JSONObject fullDict = null;
    private static JSONObject graphemesDict = null;
    private static JSONObject phonemesByLanguage = null;

    public static void setLanguageFromShortTag(Context context, String shortTag) {
        //  This method allows to set all language Globlas from short tags of language,
        //  for example 'fr' is the tag for french
        //  this tag is obtained from current locale through getResources().getConfiguration().locale;

        if (shortTag.equals("en")) language = context.getString(R.string.english_std);
        else if (shortTag.equals("fr")) language = context.getString(R.string.french_std);
        else if (shortTag.equals("it")) language = context.getString(R.string.italian_std);
        else if (shortTag.equals("es")) language = context.getString(R.string.spanish_std);
        else language = context.getString(R.string.english_std);
        setLanguageAttributes(context, language);
    }

    public static void setLanguageAttributes(Context context, String language) {
        //  This method allows to set all language Globlas from the language string,
        //  language input is the language obtained from reading SharedPreferences
        //  through preferences.getString("language", "defaultValue"));

        String englishStd = context.getString(R.string.english_std);
        String frenchStd = context.getString(R.string.french_std);
        String italianStd = context.getString(R.string.italian_std);
        String spanishStd = context.getString(R.string.spanish_std);
        String spanishGui = context.getString(R.string.spanish_gui);

        if (language.equals(englishStd)) {
            fullDictFileName = "dictionary-english-std.json";
            graphemesFileName = "graphemes-english-std.json";
            languageRulesTag = "english_rules_std";
            languageTag = "english_std";
            languageTitle = "(EN)";
        } else if (language.equals(frenchStd)) {
            fullDictFileName = "dictionary-french-std.json";
            graphemesFileName = "graphemes-french-std.json";
            languageRulesTag = "french_rules_std";
            languageTag = "french_std";
            languageTitle = "(FR)";
        } else if (language.equals(italianStd)) {
            fullDictFileName = "dictionary-italian-std.json";
            graphemesFileName = "graphemes-italian-std.json";
            languageRulesTag = "italian_rules_std";
            languageTag = "italian_std";
            languageTitle = "(IT)";
        } else if (language.equals(spanishStd)) {
            fullDictFileName = "dictionary-spanish-std.json";
            graphemesFileName = "graphemes-spanish-std.json";
            languageRulesTag = "spanish_rules_std";
            languageTag = "spanish_std";
            languageTitle = "(ES)";
        } else if (language.equals(spanishGui)) {
            fullDictFileName = "dictionary-spanish-gui.json";
            graphemesFileName = "graphemes-spanish-gui.json";
            languageRulesTag = "spanish_rules_gui";
            languageTag = "spanish_gui";
            languageTitle = "(ES-G)";
        }

        setDictionary(context);
    }

    public static void setDictionary(Context context) {
        //  Once the language is already set this method loads the dictionaries
        //  that mnemonic functions will use later

        try {
            //  This dict has all the words associated with each number from 0 to 999
            fullDict = getJsonFromAssets(context, fullDictFileName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            //  This dict has all the consonant graphemes as keys and the number that they represent as value
            //  Is used in the inverter function to anti transform the mnemonic to the original number
            graphemesDict = getJsonFromAssets(context, graphemesFileName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            //  This dict has the language rules for each language
            //  Is only used for display proposals in HELP fragment
            phonemesByLanguage = getJsonFromAssets(context, phonemesByLanguageFileName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static String getLanguageTitle() {
        return languageTitle;
    }

    public static String getLanguage() {
        return language;
    }

    public static JSONObject getFullDict() {
        return fullDict;
    }

    public static JSONObject getPhonemes() {
        return phonemesByLanguage;
    }

    public static JSONObject getGraphemes() {
        return graphemesDict;
    }

    public static JSONObject getLanguageRules() {
        JSONObject currentLanguagePhonemes = new JSONObject();

        try {
            currentLanguagePhonemes = (JSONObject) getPhonemes().get(languageRulesTag);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return currentLanguagePhonemes;
    }
}

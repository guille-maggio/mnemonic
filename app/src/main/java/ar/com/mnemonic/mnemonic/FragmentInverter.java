/*
 * <MNEMONIC - Herigone's System Mnemonics Generator>
 * Copyright (C) <2020>  <Guillermo Maggio>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package ar.com.mnemonic.mnemonic;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import org.json.JSONException;

import static ar.com.mnemonic.mnemonic.Mnemonic.inverter;

public class FragmentInverter extends Fragment {

    public FragmentInverter() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setText(String text) {
        TextView t = getView().findViewById(R.id.number_from_mnemonic);
        t.setText(text);

    }

    public String getText() {
        TextView t = getView().findViewById(R.id.mnemonic_words);
        return t.getText().toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_inverter, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setBackgroundResource(R.drawable.background_left);


        view.findViewById(R.id.back_to_main).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        view.findViewById(R.id.transform_to_number).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputText = "";
                String outputNumbers = "";
                inputText = getText().toLowerCase();

                try {
                    outputNumbers = inverter(inputText, LangHelper.getGraphemes());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                setText(outputNumbers);
            }
        });

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int movement = TouchHelper.checkMovement(event);
                switch (movement) {
                    case (TouchHelper.RIGHT_TO_LEFT):
                        view.findViewById(R.id.back_to_main).performClick();
                        return true;
                    default:
                        return true;
                }
            }
        });
    }
}
